<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Email Helpers | Register listeners
    |--------------------------------------------------------------------------
    |
    | Map the standard laravel events to actions that need to take place
    */
   'listeners' => [
        \Illuminate\Mail\Events\MessageSending::class => [
            # Always add specified email address as carbon copy.
            # Add email list to 'cc' field below
            \BinaryCats\LaravelMailHelpers\Sending\CarbonCopy::class,

            # Always add specified email address as balck carbon copy.
            # Add email list to 'bcc' field below
            \BinaryCats\LaravelMailHelpers\Sending\BlackCarbonCopy::class,

            # Parse the HTML to apply all the classes when sending HTML emails
            \BinaryCats\LaravelMailHelpers\Sending\InlineCss::class,

            # Overwrite Sender email
            \BinaryCats\LaravelMailHelpers\Sending\SenderOverwrite::class,
        ],

        \Illuminate\Mail\Events\MessageSent::class => [
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Email Helpers | List Carbon Copy recipients
    |--------------------------------------------------------------------------
    |
    | Each elemenet of the array will be BCC'ed to the email
    */
    'cc' => [
        # 'example@example.com',
    ],

    /*
    |--------------------------------------------------------------------------
    | Email Helpers | List Black Carbon Copy recipients
    |--------------------------------------------------------------------------
    |
    | Each elemenet of the array will be BCC'ed to the email
    */
    'bcc' => [
        # 'example@example.com',
    ],

    /*
    |--------------------------------------------------------------------------
    | Email Helpers | Overwrite the sender
    |--------------------------------------------------------------------------
    |
    | Whenever you are sending from the root domain but your transport is on the subdomain
    | you may need to set the sender different to From field, sent in config.mail.from
    */
    'sender' => env('MAIL_FROM_ADDRESS'),
];
