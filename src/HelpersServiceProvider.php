<?php

namespace BinaryCats\LaravelMailHelpers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class HelpersServiceProvider extends ServiceProvider
{
    /**
     * Location of the provider
     *
     * @var string
     */
    protected $path = __DIR__;

    /**
     * Name of the package
     *
     * @var string
     */
    protected $name = 'mail-helpers';

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();

        parent::boot();
    }

    /**
     * Register the service provider
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();

        parent::register();
    }

    /**
     * Get the events and handlers.
     *
     * @return array
     */
    public function listens()
    {
        return config("{$this->name}.listeners");
    }

    /**
     * Merge the config for the repo
     *
     * @return $this
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom("{$this->path}/../config/{$this->name}.php", $this->name);

        return $this;
    }

    /**
     * Publish config for the repo
     *
     * @return $this
     */
    protected function publishConfig()
    {
        $this->publishes([
            "{$this->path}/../config/{$this->name}.php" => config_path("{$this->name}.php")
        ], 'config');

        return $this;
    }
}
