<?php

namespace BinaryCats\LaravelMailHelpers\Sending;

use Swift_Message;
use Illuminate\Mail\Events\MessageSending;

class BlackCarbonCopy
{
    /**
     * Record Email sent
     *
     * @param  MessageSending $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $this->setGlobalBcc($event->message);
    }

    /**
     * Set Global BCC
     *
     * @param  Swift_Message $message
     * @return Swift_Message $message
     */
    protected function setGlobalBcc(Swift_Message $message) : Swift_Message
    {
        # Add Silent BCC
        $bcc = array_merge((array) $message->getBcc(), $this->blackCarbonCopies());
        # merge message info
        $message->setBcc($bcc);
        # return
        return $message;
    }

    /**
     * Get the BCCs that we want to add
     *
     * @return array
     */
    protected function blackCarbonCopies() : array
    {
        return config('mail-helpers.bcc', []);
    }
}
