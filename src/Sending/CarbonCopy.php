<?php

namespace BinaryCats\LaravelMailHelpers\Sending;

use Swift_Message;
use Illuminate\Mail\Events\MessageSending;

class CarbonCopy
{
    /**
     * Record Email sent
     *
     * @param  MessageSending $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $this->addGlobalCarbonCopy($event->message);
    }

    /**
     * Add Global CC
     *
     * @param  Swift_Message $message
     * @return Swift_Message $message
     */
    protected function addGlobalCarbonCopy(Swift_Message $message) : Swift_Message
    {
        # Add Global CC
        $carbonCopy = array_merge((array) $message->getCc(), $this->carbonCopies());
        # merge message info
        $message->setCc($carbonCopy);
        # return
        return $message;
    }

    /**
     * Get the BCCs that we want to add
     *
     * @return array
     */
    protected function carbonCopies() : array
    {
        return config('mail-helpers.cc', []);
    }
}
