<?php

namespace BinaryCats\LaravelMailHelpers\Sending;

use Swift_Message;
use Illuminate\Mail\Events\MessageSending;

class SenderOverwrite
{
    /**
     * Record Email sent
     *
     * @param  MessageSending $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $this->setSender($event->message);
    }

    /**
     * Set Sender
     *
     * @param  Swift_Message $message
     * @return Swift_Message $message
     */
    protected function setSender(Swift_Message $message) : Swift_Message
    {
        if ($sender = $this->sender()) {
            $message->setSender($sender);
        }

        return $message;
    }

    /**
     * Load the sender from the key
     *
     * @return mixed
     */
    protected function sender()
    {
        return config('mail-helpers.sender', []);
    }
}
