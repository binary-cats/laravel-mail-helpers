<?php

namespace BinaryCats\LaravelMailHelpers\Sending;

use Swift_Message;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Mail\Events\MessageSending;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class InlineCss
{
    /**
     * Bind the implementation
     *
     * @var TijsVerkoyen\CssToInlineStyles\CssToInlineStyles
     */
    protected $inliner;

    /**
     * Create new Listener
     *
     * @param CssToInlineStyles $inliner
     */
    public function __construct(CssToInlineStyles $inliner)
    {
        $this->inliner = $inliner;
    }

    /**
     * Record Email sent
     *
     * @param  MessageSending $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $this->inlineCss($event->message);
    }

    /**
     * Convert Inline CSS
     *
     * @param  Swift_Message $message
     * @return Swift_Message $message
     */
    protected function inlineCss(Swift_Message $message)
    {
        # Conver the Email body
        $body = $this->inliner->convert($message->getBody());
        # Replace the body with results of conversion
        $message->setBody($body);
        # Return message
        return $message;
    }
}
