# A small collection of everyday Email Helpers

Small Laravel package adds a collection of email helpers

## Installation and usage

This package requires PHP 7 and Laravel 5.6 or higher.

``` bash
php composer require binary-cats/laravel-mail-helpers
```

and then publish the config:
``` bash
php artisan vendor:publish --provider="BinaryCats\LaravelMailHelpers\HelpersServiceProvider"
```
config will add `mail-helpers.php` to your `config` folder.

## Configuration

Right out of the box the following classes are enabled:
### CarbonCopy
Add all the emails in `mail-helpers.cc` to all emails sent. None by default.

### BlackCarbonCopy
Add all the emails in `mail-helpers.bcc` to all emails sent. None by default.

### InlineCss
Apply inline CSS style to the HTML body of your message. This results in a larger email size, however,
it appears that inline styles are respected way more by any email client

### SenderOverwrite
Whenever you are sending emails from a subdomain (like, `mg.example.com`) but your from is a root domain (`example.com`) some mail servers consider that spoofing and reject the envelope with `550 Verification failed for <...> Sender verify failed` or the likes.

Resetting the sender may alleviate that problem.

By default it is set to the same value as the `mail.from` config key.

## Testing

Run the tests with:

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security-related issues, please email info@binarycats.io instead of using the issue tracker.

## Credits

- [Cyrill Kalita](https://bitbucket.org/cyrillkalita)
- [All Contributors](../../contributors)

## Support us
Binary Cats is a web service agency based in Roselle, Illinois.

Does your business depend on our contributions? Reach out!
All pledges will be dedicated to allocating workforce on maintenance and new awesome stuff.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
